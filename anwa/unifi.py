import json
import sys
from dataclasses import dataclass
from typing import List, Optional

import pydantic
from icecream import ic


# config_network
class c_network(pydantic.BaseModel):
    netmask: str | None = ""
    ip: str | None = ""
    dns1: str | None = ""
    dns2: str | None = ""
    type: str | None = ""
    dnssuffix: str | None = ""
    gateway: str | None = ""

    class Config:
        extra = "ignore"


# port_table
class port(pydantic.BaseModel):
    port_idx: int | None = 0
    media: str | None = ""
    port_poe: bool | None = False
    poe_caps: int | None = 0
    speed_caps: int | None = 0
    op_mode: str | None = ""
    forward: str | None = ""
    port_security_enabled: bool | None = False
    stp_port_mode: bool | None = False
    native_networkconf_id: str | None = ""
    name: str | None = ""
    autoneg: bool | None = False
    tagged_vlan_mgmt: str | None = ""
    enable: bool | None = False
    masked: bool | None = False
    aggregated_by: bool | None = False
    full_duplex: bool | None = False
    is_uplink: bool | None = False
    jumbo: bool | None = False
    speed: int | None = 0
    stp_state: str | None = ""
    up: bool | None = False

    class Config:
        extra = "ignore"


# uplink
class uplink(pydantic.BaseModel):
    uplink_mac: str | None = ""
    uplink_device_name: str | None = ""
    uplink_remote_port: int | None = 0
    port_idx: int | None = 0
    type: str | None = ""

    class Config:
        extra = "ignore"


# port_overrides
class p_overrides(pydantic.BaseModel):
    port_idx: int | None = 0
    forward: str | None = ""
    native_networkconf_id: str | None = ""
    name: str | None = ""
    # port_security_mac_address: []
    autoneg: bool | None = False
    tagged_vlan_mgmt: str | None = ""

    class Config:
        extra = "ignore"


# device
class dev(pydantic.BaseModel):
    type: str | None = ""
    board_rev: int | None = 0
    setup_id: str | None = ""
    hw_caps: int | None = 0
    snmp_contact: str | None = ""
    model: str | None = ""
    ip: str | None = ""
    version: str | None = ""
    site_id: str | None = ""
    name: str | None = ""
    _id: str | None = ""
    mgmt_network_id: str | None = ""
    mac: str | None = ""
    architecture: str | None = ""
    has_fan: bool | None = False
    model_in_eol: bool | None = False
    has_temperature: bool | None = False
    snmp_location: str | None = ""
    kernel_version: str | None = ""
    serial: str | None = ""
    device_id: str | None = ""
    uplink: uplink | None = uplink
    config_network: c_network | None = c_network
    port_table: list[port] | None = None
    port_overrides: list[p_overrides] | None = None
    has_speaker: bool | None = False

    class Config:
        extra = "ignore"


class netw(pydantic.BaseModel):
    setting_preference: str | None = ""
    dhcpdv6_dns_auto: bool | None = False
    purpose: str | None = ""
    dhcpd_leasetime: int | None = 0
    ipv6_pd_stop: str | None = ""
    igmp_snooping: bool | None = False
    dhcpd_gateway_enabled: bool | None = False
    dhcpd_time_offset_enabled: bool | None = False
    dhcpguard_enabled: bool | None = False
    dhcpd_dns_1: str | None = ""
    ipv6_client_address_assignment: str | None = ""
    dhcpd_start: str | None = ""
    dhcpd_unifi_controller: str | None = ""
    ipv6_ra_preferred_lifetime: int | None = 0
    ipv6_ra_enabled: bool | None = False
    dhcpd_stop: str | None = ""
    enabled: bool | None = False
    domain_name: str | None = ""
    dhcpd_enabled: bool | None = False
    vlan: int | None = 0
    ip_subnet: str | None = ""
    dhcpd_wpad_url: str | None = ""
    ipv6_interface_type: str | None = ""
    dhcpd_dns_2: str | None = ""
    networkgroup: str | None = ""
    dhcpdv6_start: str | None = ""
    vlan_enabled: bool | None = False
    ipv6_setting_preference: str | None = ""
    dhcpdv6_stop: str | None = ""
    is_nat: bool | None = False
    dhcpd_dns_enabled: bool | None = False
    dhcpdv6_enabled: bool | None = False
    gateway_type: str | None = ""
    ipv6_ra_priority: str | None = ""
    # nat_outbound_ip_addresses: []
    dhcp_relay_enabled: bool | None = False
    dhcpd_boot_enabled: bool | None = False
    ipv6_pd_start: str | None = ""
    upnp_lan_enabled: bool | None = False
    dhcpd_ntp_enabled: bool | None = False
    name: str | None = ""
    site_id: str | None = ""
    dhcpdv6_leasetime: int | None = 0
    mdns_enabled: bool | None = False
    _id: str | None = ""
    lte_lan_enabled: bool | None = False
    dhcpd_tftp_server: str | None = ""
    auto_scale_enabled: bool | None = False

    class Config:
        extra = "ignore"


class UnifiDevice:
    def __init__(self, device_name):
        self.device_name = device_name
        self.devices = self.load_devices()
        self.device = self.find_device_by_name(device_name)
        self.networks = self.load_networks()

        if self.device:
            ic(type(self.device))

    def find_device_by_name(self, name):
        for device in self.devices:
            if device.name == name:
                return device
        return None

    def get_port_by_id(self, id):
        # Stellen Sie sicher, dass port_table existiert
        if self.device and self.device.port_table:
            for port in self.device.port_table:
                if port.port_idx == id:
                    return port
        return None

    def get_port_or_by_id(self, id):
        # Stellen Sie sicher, dass port_table existiert
        if self.device and self.device.port_overrides:
            for port in self.device.port_overrides:
                if port.port_idx == id:
                    return port
        return None

    def get_network_by_id(self, id):
        # Stellen Sie sicher, dass port_table existiert
        for network in self.networks:
            if network._id == id:
                return network
        return None

    def load_devices(self):
        with open("unifi/device.json") as f:
            response = json.load(f)
            return [dev(**item) for item in response]

    def load_networks(self):
        with open("unifi/networkconf.json") as f:
            response = json.load(f)
            return [netw(**item) for item in response]


if __name__ == "__main__":
    ic("main called!")
