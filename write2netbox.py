import json
import os

from icecream import ic
from pynetbox.core.response import RecordSet
from pynetbox.models.dcim import Devices

from anwa import common


def get_config_values():
    try:
        # config = common.get_config()
        return common.get_config()
    except Exception as e:
        print(f"Error retrieving configuration values: {e}")
        exit(1)


def get_netbox_device(dev: str):
    return netbox.dcim.devices.get(name=dev)


def get_netbox_devices():
    return netbox.dcim.devices.all()


def get_netbox_interfaces():
    return netbox.dcim.interfaces.all()


def save_to_file(dir, filename, data):
    os.makedirs(dir, exist_ok=True)
    if isinstance(data, Devices):
        data_list = data.serialize()
    elif isinstance(data, RecordSet):
        data_list = [dict(dat) for dat in data]
    with open(os.path.join(dir, filename), "w") as json_file:
        json.dump(data_list, json_file, indent=2)


def save_netbox_devices():
    save_to_file("netbox", "devices.json", get_netbox_devices())


def save_netbox_device(dev: str):
    save_to_file("netbox", f"{dev}.json", get_netbox_device(dev))


def save_netbox_interfaces():
    save_to_file("netbox", "interfaces.json", get_netbox_interfaces())


def delete_netbox_device_type(device_type_model: str):
    # Retrieve the device type object by name
    device_types = netbox.dcim.device_types.filter(model=device_type_model)

    # Check if the object exists and delete it
    if device_types:
        ic(type(device_types))
        for device_type in device_types:
            device_type.delete()
            ic(f"Device Type '{device_type_model}' has been deleted.")
    else:
        ic(f"No Device Type found with the name '{device_type_model}'.")


# if __name__ == "__main__":
config = get_config_values()
netbox = common.get_netbox_instance(config)

ic(netbox.dcim.manufacturers.get(name="Ubiquiti").id)

# Write some endpoints to disk
save_netbox_devices()
save_netbox_device("Python-Test")
save_netbox_interfaces()


interfaces = netbox.dcim.interfaces.filter(device_id=64)

for interface in interfaces:
    print(f"Interface: {interface.name}, ID: {interface.id}")
    interface.description = "Neue Beschreibung"
    interface.enabled = True
    interface.name = f"Port {interface.label} aus Unifi"
    interface.speed = 1000000
    interface.duplex = "full"
    # interface.duplex = None
    # access - Access
    # tagged - Tagged
    # tagged-all - Tagg
    interface.mode = "access"
    interface.mgmt_only = False

    if interface.type.value == "10gbase-x-sfpp":
        interface.poe_mode = None
        interface.poe_type = None
    else:
        #        interface.poe_mode = None
        interface.poe_mode = "pse"
        #        interface.poe_mode = 'pd'
        # type1-ieee802.3af	802.3af (Type 1)
        # type2-ieee802.3at	802.3at (Type 2)
        # type3-ieee802.3bt	802.3bt (Type 3)
        # type4-ieee802.3bt	802.3bt (Type 4)
        #        interface.poe_type = None
        #        interface.poe_type = 'type4-ieee802.3bt'
        interface.poe_type = "type2-ieee802.3at"

    interface.untagged_vlan = 2
    interface.tagged_vlans = []
    #    interface.untagged_vlan = 7
    #    interface.tagged_vlans = [7, 2]

    # Speichern der Änderungen
    interface.save()

# Delete a Device-Type
# delete_netbox_device_type('USW-PRO-24 PyTest')


# unit_height = 2
# ic(netbox.dcim.device_types.create(
#    manufacturer=netbox.dcim.manufacturers.get(name='Ubiquiti').id,
#    model='device-type-name1',
#    slug='device-type-slug1',
#    # optional field - requred if creating a device type to be used by child devices
#    #    subdevice_role='child or parent',
#    u_height=unit_height  # can only equal 0 if the device type is for a child device - requires subdevice_role='child' if that is the case
#    # custom_fields={'cf_1': 'custom data 1'}
# ))
