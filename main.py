import json
import os
from argparse import ArgumentParser, Namespace

import requests
from icecream import ic

from anwa import common

requests.packages.urllib3.disable_warnings()

# Global constants
UNIFI_HEADERS = {"Accept": "application/json", "Content-Type": "application/json"}

ENDPOINT = [
    "stat/ccode",
    "stat/sysinfo",
    "stat/sta",
    "stat/device-basic",
    "stat/device",
    # "stat/routing",
    "rest/routing",
    "rest/firewallrule",
    "rest/firewallgroup",
    "rest/wlanconf",
    "rest/tag",
    "stat/rogueap",
    # "stat/dynamicdns",
    "rest/dynamicdns",
    "rest/portconf",
    "rest/portforward",
    "stat/sdn",
    "rest/portconf",
    "rest/networkconf",
]


parser = ArgumentParser(
    description="This script reads device settings from a Unifi Controller and updates a NetBox instance. "
    "It is designed to synchronize device information between Unifi and NetBox, ensuring that "
    "NetBox contains up-to-date information about network devices managed by Unifi."
)
parser.add_argument(
    "-e", "--export", metavar="[path]", type=str, help="Path to export json file to"
)
args: Namespace = parser.parse_args()


def get_config_values():
    try:
        config = common.get_config()
        unifi_auth = common.get_unifi_auth_data(config)
        unifi_auth_url = common.get_unifi_auth_url(config)
        unifi_api_url = common.get_unifi_api_url(config)
        return unifi_auth, unifi_auth_url, unifi_api_url
    except Exception as e:
        print(f"Error retrieving configuration values: {e}")
        exit(1)


def create_unifi_session(unifi_auth_url, unifi_auth):
    try:
        s = requests.Session()
        r = s.post(unifi_auth_url, headers=UNIFI_HEADERS, json=unifi_auth, verify=False, timeout=10)
        r.raise_for_status()
        ic(r.status_code)
        return s
    except requests.RequestException as e:
        print(f"Error creating the session: {e}")
        exit(1)


def add_json_extension(input: str) -> str:
    # Split the string at the last slash
    parts = input.rsplit("/", 1)

    if len(parts) == 2:
        # parts[1] contains the part after the last slash
        out = parts[1] + ".json"
    else:
        # If no slash is found or if the slash is at the end
        out = input + ".json"

    return out


def export_json(session, endpoints, unifi_api_url):
    for input_string in endpoints:
        try:
            filename = add_json_extension(input_string)
            response = session.get(
                unifi_api_url + input_string, headers=UNIFI_HEADERS, verify=False, timeout=10
            )
            response.raise_for_status()
            data = response.json()
            # ic(args.export, filename)
            if args.export:
                save_to_file(args.export, filename, data["data"])
        except requests.RequestException as e:
            print(f"Error in request {input_string}: {e}")
        except json.JSONDecodeError as e:
            print(f"JSON parsing error in {input_string}: {e}")


def save_to_file(dir, filename, data):
    os.makedirs(dir, exist_ok=True)
    with open(os.path.join(dir, filename), "w") as json_file:
        json.dump(data, json_file, indent=2)


# Additional functions and global variables ...


if __name__ == "__main__":
    unifi_auth, unifi_auth_url, unifi_api_url = get_config_values()
    session = create_unifi_session(unifi_auth_url, unifi_auth)
    export_json(session, ENDPOINT, unifi_api_url)
