import os
from configparser import ConfigParser
from configparser import Error as ConfigParserError

import pynetbox

DEFAULT_CONFIG_PATH = "unifi2netbox.conf"


def get_config():
    config = ConfigParser()

    # Check if the file exists
    if not os.path.isfile(DEFAULT_CONFIG_PATH):
        raise Exception(f"Configuration file not found: {DEFAULT_CONFIG_PATH}")

    try:
        config.read(DEFAULT_CONFIG_PATH)
        return config
    except ConfigParserError as e:
        raise Exception(f"Failed to parse configuration file: {DEFAULT_CONFIG_PATH} - {e}")


def get_unifi_base_url(config):
    return f'https://{config.get("unifi", "url")}/'


def get_unifi_auth_url(config):
    base_url = get_unifi_base_url(config)
    return f"{base_url}api/auth/login"


def get_unifi_auth_data(config):
    username = config.get("unifi", "username")
    password = config.get("unifi", "password")
    return {"username": username, "password": password}


def get_unifi_api_url(config):
    base_url = get_unifi_base_url(config)
    return f'{base_url}proxy/network/api/s/{config.get("unifi", "site")}/'


def get_netbox_instance(config):
    url = config.get("netbox", "url")
    port = config.get("netbox", "port")
    token = config.get("netbox", "token")
    use_ssl = config.getboolean("netbox", "use_ssl", fallback=False)

    # Validation of configuration parameters
    if not url or not port or not token:
        raise ValueError("NetBox configuration values are missing or invalid.")

    return pynetbox.api(f"http://{url}:{port}", token=token)
