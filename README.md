# Unifi2Netbox

## Description

The project is designed to synchronize a UniFi setup with NetBox.

<!--
## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.
-->

## Installation

Linux

```bash
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
```

Windows

```powershell
py -m venv .venv
.\venv\Scripts\activate
pip install -r requirements.txt
```

## Usage

Currently, you can simply run main.py, and it will read from the UniFi UDM Pro. Each endpoint of the API will be saved as a JSON file in the export folder.

## Support

If you would like to support my work, feel free to open an issue and provide any code, advice, or suggestions you have in mind. Your contributions are highly appreciated!

## Roadmap

- [x] Retrieve data from UDM Pro and save it as JSON files on the disk.
- [ ] Read JSON data from disk and create a model for each device, including all necessary information.
- [ ] Update devices and interfaces in NetBox.
- [ ] Directly create a model from the UniFi response (no saving necessary).
- [x] Make the saving of JSON data optional, with an argument: --export \[folder\].
- [ ] Upload a device template to NetBox if the device is not already present.
- [ ] Create a device from the template and populate it with values from the model.
- [ ] Consider developing a GUI.
- [ ] Display changes in the GUI and require confirmation before updating.
- [ ] Read from NetBox and check for changes. Update UniFi accordingly.

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

## Authors and acknowledgment

Maintainer: andreas@anwa-soft.de

## License

[MIT License](https://choosealicense.com/licenses/mit/#)

## Project status

I'm just starting with Python, and I love it. So, please don't criticize my code harshly; instead, help me improve it if you can. This project was initiated to assist in managing my UniFi setup and to ensure proper documentation in Netbox. Everyone is welcome to use and enhance it.
